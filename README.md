# README #


### Silent Auction ###

This basic python script resolveAuction.py reads an inventory and bidding responses (e.g. inventory.csv and responses.csv) and resolves the  auction in [Vickery format](https://en.wikipedia.org/wiki/Vickrey_auction), similar to eBay.


### Setting Up ###

After installing [python](https://www.python.org/), and the [numpy](http://www.numpy.org/) and [pandas](https://pandas.pydata.org/) packages (e.g. via [pip](https://pypi.python.org/pypi/pip)), you can run as:

$ python resolveAuction.py

which will write outcome.csv, containing the winning bids of the items that sold.


### Quick Start ###

For reference, there are example csv files included in the repo (inventory.csv and responses.csv) that you can test on first that will produce the following results in outcome.csv:

|ID|Board Game|Buyer|Auction Price|Seller|
| --- | ------------- | ------ | ----- | ------------- |
|11|Dominant Species|Kaylee|70|Bugs|
|15|Ares Project|Jayne|10|Elmer|
|20|Civilization|Simon|20|Foghorn|
|25|Coup Rebellion|Jayne|15|Marvin|
|28|One Deck Dungeon (P&P)|Jayne|1|Sam|
|29|1000 Drinking Games|Simon|0|Sam|
|30|What the F*ck|Kaylee|1|Sam|
|31|Wits & Wagers Family Edition|Jayne|0|Sam|
|33|Origami Chess: Cats vs Dogs|Kaylee|10|Sam|
|34|Starfarers of Catan|Kaylee|25|Sam|
|37|Cities and Knights of Catan|Simon|20|Bugs|

When building your own auction, [Google Sheets](https://www.google.com/sheets) works well at collaboratively building an inventory by allowing anyone with the link to edit the document.

Likewise, [Google Forms](https://www.google.com/forms) works well when soliciting bids, and can produce csv output like that found in responses.csv.


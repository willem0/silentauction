import sys
import pandas
import math
import numpy as np

#file input to dataframe format
if len(sys.argv)>1:
	bidsFile = sys.argv[1]
else:
	bidsFile = "responses.csv"
bidsDF = pandas.read_csv(bidsFile)

if len(sys.argv)>2:
	inventoryFile = sys.argv[2]
else:
	inventoryFile = "inventory.csv"
inventory = pandas.read_csv(inventoryFile)

# parse to lists
IDs = list(inventory["ID"])
minBids = list(inventory["Minimum Bid"])
games = list(inventory["Board Game"])
sellers = list(inventory["Seller Name"])

names = list(bidsDF["Name"])
#phones = list(bidsDF["Qualcomm ID or phone number"])
bids1 = list(bidsDF["Bid for Item #1"])
bids2 = list(bidsDF["Bid for Item #2"])
bids3 = list(bidsDF["Bid for Item #3"])
ids1 = list(bidsDF["Auction ID #1"])
ids2 = list(bidsDF["Auction ID #2"])
ids3 = list(bidsDF["Auction ID #3"])
bids = [bids1, bids2, bids3]
ids = [ids1, ids2, ids3]

# build dictionary of minimum bids
dMinBids = dict()
dBids = dict()
dBidders = dict()
dSellers = dict()
dGames = dict()
for n in range(len(IDs)):
	idx = IDs[n]
	if math.isnan(idx) or math.isnan(minBids[n]):
		continue
	dMinBids[idx] = int(minBids[n])
	dBids[idx] = [0]
	dBidders[idx] = [0]
	dSellers[idx] = sellers[n]
	dGames[idx] = games[n]

# build dictionary of legitimate bids and corresponding bidders
for n in range(len(names)):
	for j in range(len(ids)):
		idx = ids[j][n]
		bidx = bids[j][n]
		if idx not in list(dMinBids.keys()) or math.isnan(bidx):
			continue
		bidx = int(bidx)
		if bidx<dMinBids[idx]:
			continue
		print("A bid of %d for auction ID %d was placed by %s" %(bidx, idx, names[n]) )
		dBids[idx] += [bidx]
		dBidders[idx] += [n]
print('')

#apend each auction with the minimum bid
for idx in dBids.keys():
	dBids[idx] += [dMinBids[idx]]
	dBidders[idx] += [0]

#resolve each auction
orphans = []
print("ID  | board game | buyer | auction price | seller")
fid = open('outcome.csv','w')
fid.write("ID,Board Game,Buyer,Auction Price,Seller\n")
for idx in dBids.keys():
	bidList = dBids[idx]
	bidListNeg = list(-np.array(bidList))
	if len(bidList) == 2:
		orphans += [idx]
		continue
	nx = np.argsort(bidListNeg) # negate to sort in reverse order
	winBid = bidList[nx[1]]
	winID = dBidders[idx][nx[0]]
	winner = names[ winID ]
	# winnerInfo = winner + ": " + str(phones[winID])
	outTuple = (idx, dGames[idx], winner, winBid, dSellers[idx])
	print("%d         %s      %s   %d          %s  " % outTuple)
	fid.write("%d,%s,%s,%d,%s\n" % outTuple)
fid.close()
